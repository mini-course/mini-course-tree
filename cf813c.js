function treeNode(id) {
  this.adj = [];
  this.parent = null;
  this.id = id;
  this.depth = 0;
  this.down = 0;

  this.dfsFrom = function() {
    this.parent = null;
    this.depth = 0;
		this.dfs();
  }

  this.dfs = function() {
    this.down = 0;
    this.adj.forEach((u) => {
      if (u == this.parent) return;
      u.parent = this;
      u.depth = this.depth + 1;
      u.dfs();
      this.down = Math.max(this.down, u.down + 1);
    });
  };

  this.isAdjacent = function(u) {
    return this.adj.some((v) => {
      return v.id == u;
    });
  };
}

function tree(n) {
  this.n = n;
  this.node = [];
  this.player = [-1, -1];
  this.round = 0;

  for (var i = 0; i < n; i++)
    this.node.push(new treeNode(i));

  this.addEdge = function(u, v) {
    this.node[v].adj.push(this.node[u]);
    this.node[u].adj.push(this.node[v]);
  };

  this.startGame = function(u, v) {
    this.player = [u, v];
  };

  this.move = function(u) {
    if (u != this.player[this.round % 2] && !this.node[u].isAdjacent(this.player[this.round % 2]))
      throw 'Player can only move to adjacent nodes or stay still.';
    this.round += 1;
    if (u === this.player[this.round % 2]) return this.round;
    this.player[1 - this.round % 2] = u; 
    return -1;
  };

  this.distFrom = function(u) {
    this.node[u].dfsFrom(); 
    var dist = [];
    for (var i = 0; i < this.n; i++) dist.push(this.node[i].depth);
    return dist;
  };

  this.downFrom = function(u) {
    this.node[u].dfsFrom(); 
    var down = [];
    for (var i = 0; i < this.n; i++) down.push(node[i].down);
    return down;
  };
}

function optimalMaximizer(g) {
  var dist1 = g.distFrom(g.player[0]);
  var dist2 = g.distFrom(g.player[1]);
  var down2 = g.downFrom(g.player[1]);

  var res = -1;
  for (var i = 0; i < g.n; i++) {
    if (dist1[i] <= dist[2]) {
      if (res == -1 || down2[i] > down2[res]) res = i;
    }
  }
  if (res == -1) throw 'res == -1';
  var u = g.player[0];
  for (var i = 0; i < g.node[u].adj.length; i++) {
    var v = g.node[u].adj[i].id;
    if (dist1[v] == dist1[u] - 1) return v;
  }
  return u;
}

function optimalMinimizer(g) {
  var dist = g.distFrom(g.player[0]);
	console.log(dist);
  var u = g.player[1], x = g.player[0];
  for (var i = 0; i < g.node[u].adj.length; i++) {
    var v = g.node[u].adj[i].id;
    if (dist[v] == dist[u] - 1) return v;
  }
  return u;
}

function test() {
  g = new tree(5);
  g.addEdge(0, 1);
  g.addEdge(0, 2);
  g.addEdge(2, 3);
  g.addEdge(2, 4);
}
