Javascript games based on <https://atcoder.jp/contests/agc033/tasks/agc033_c> and <https://codeforces.com/problemset/problem/813/C>, based on [d3.js](https://d3js.org/).

Orginally written as material for an Algorithms mini course for high school students w/ Prof. P.J. Cheng at National Taiwan University.

## License

Unlicense.
