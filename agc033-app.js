function startGame(n, gameMode) {
  document.getElementById('menu').style.display = 'none';
  document.getElementById('game').style.display = '';
  var g = new tree(n);

  var seed = parseInt(document.getElementById('input-seed').value, 10);
  var links = randtree(n, seed).map((edge) => ({ source: g.node[edge.source], target: g.node[edge.target] }));

  links.forEach((link, i) => {
    g.addEdge(link.source.id, link.target.id);
  });

  var result = d3.select('#result');
  var svg = d3.select('svg');

  var edge = svg
    .append('g')
    .attr('class', 'link')
    .selectAll('line');
  var circle = svg
    .append('g')
    .attr('class', 'node')
    .selectAll('g');

  var first = true;
  var gameState = gameState == 0 ? 'computerTurn' : 'playerTurn';

  var circleDrag = () => {
    function dragstarted(d) {
      if (!d3.event.active) simulation.alphaTarget(0).restart();
      d.fx = d.x;
      d.fy = d.y;
    }
    function dragged(d) {
      d.fx = d3.event.x;
      d.fy = d3.event.y;
    }
    function dragended(d) {
      if (!d3.event.active) simulation.alphaTarget(0.01).restart();
      d.fx = null;
      d.fy = null;
    }
    return d3.drag()
      .on("start", dragstarted)
      .on("drag", dragged)
      .on("end", dragended);
  };

  function redraw() {
    edge = edge.data(links);
    edge.exit().remove();
    var edgeEnter = edge
      .enter()
      .append('line')
      .attr('stroke-width', 2)
      .attr('stroke', 'black');
    edge = edgeEnter.merge(edge);

    circle = circle.data(g.node);
    circle.exit().remove();
    var circleEnter = circle
      .enter()
      .append('g')
      .on('mousedown', (d) => {
        if (gameMode != 2) { // PvE
          if (gameState === 'playerTurn') {
            if (!d.label) {
              // alert('Follow the rules, moron!');
              return;
            }
            gameState = 'computerTurn';
            g.removeVertex(d.id);
            redraw();
            if (g.terminate()) {
              alert('You Won');
              gameState = 'end';
              window.location.reload(false);
            } else {
              setTimeout(() => {
                gameState = 'playerTurn';
                optimalPlayer(g);
                redraw();
                if (g.terminate() && gameState != 'end') {
                  alert('You Lost');
                  gameState = 'end';
                  window.location.reload(false);
                }
              }, 1000);
            }
          }
        } else { // PvP
          if (!d.label) {
            alert('Follow the rules, moron!');
            return;
          }
          g.removeVertex(d.id);
          redraw();
          if (g.terminate()) {
            var playerId = gameState == 'playerTurn' ? 1 : 2;
            alert('Player ' + playerId + ' won');
            window.location.reload(false);
          }
          gameState = gameState == 'playerTurn' ? 'computerTurn' : 'playerTurn';
        }
      })
      .merge(circle);
    circleEnter
      .append('circle')
      .attr('r', 20)
      .attr('fill', (d) => (d.id == g.lastRemovedId ? 'green' : 'white'))
      .attr('stroke', (d) => (d.id == g.lastRemovedId ? 'green' : 'black'))
      .call(circleDrag());
    circleEnter
      .append('text')
      .attr('x', -5)
      .attr('y', 5)
      .text((d) => d.label);
    circle = circleEnter;

    if (first && gameMode == 0) {
      first = false;
      setTimeout(() => {
        gameState = 'playerTurn';
        optimalPlayer(g);
        redraw();
      }, 1000);
    }
  }

  function tick() {
    circle.attr('transform', (d) => `translate(${d.x},${d.y})`);
    edge
      .attr("x1", (d) => d.source.x)
      .attr("y1", (d) => d.source.y)
      .attr("x2", (d) => d.target.x)
      .attr("y2", (d) => d.target.y);
  }

  var center = {x: svg.attr('width') / 2, y: svg.attr('height') / 2};
  var simulation = d3
    .forceSimulation()
    .nodes(g.node)
    .force('link', d3.forceLink(links))
    .force('charge', d3.forceManyBody().strength(-512).distanceMax(512))
    .force('center', d3.forceCenter(center.x, center.y))
    .on('tick', tick);
  function init() {
    svg.attr('width', window.innerWidth);
    svg.attr('height', window.innerHeight);
    center = {x: svg.attr('width') / 2, y: svg.attr('height') / 2};
    simulation.restart();
    redraw();
  }
  init();

  drag = () => {
    function dragged(d) {
      center.x += d3.event.dx;
      center.y += d3.event.dy; 
      simulation.force('center', d3.forceCenter(center.x, center.y)).restart();
    }
    return d3.drag().on("drag", dragged);
  };
  svg.call(drag());

  window.onload = window.onresize = init;
}
