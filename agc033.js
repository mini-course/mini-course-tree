function treeNode(id) {
  this.adj = [];
  this.parent = null;
  this.label = 1;
  this.id = id;
  this.depth = 0;

  this.dfsFrom = function(func) {
    this.parent = null;
    func(this);
    this.dfs(func);
  }

  this.dfs = function(func) {
    this.adj.forEach((u) => {
      if (u == this.parent) return;
      if (u.label == 0) return;
      u.parent = this;
      func(u);
      u.dfs(func);
    });
  };
}

function tree(n) {
  this.n = n;
  this.node = [];
  for (var i = 0; i < n; i++)
    this.node.push(new treeNode(i));
  this.lastRemovedId = -1;

  this.addEdge = function(u, v) {
    this.node[u].adj.push(this.node[v]);
    this.node[v].adj.push(this.node[u]);
  };

  this.debug = function() {
    console.log('debug');
    this.node.forEach((u) => { console.log(u.id + ' ' + u.label); });
  };

  this.removeVertex = function(v) {
    if (this.node[v].label == 0) throw 'Removing vertex with label = 0.';
    this.lastRemovedId = v;
    console.log('removing ' + v);
    console.log('before');
    this.debug();
    this.node[v].dfsFrom((u) => {
      if (u.parent != null) u.parent.label += u.label;
      u.label = 0;
    });
    console.log('after');
    this.debug();
  };

  this.findDiameter = function() {
    this.debug();
    var root = this.node.find((u) => { return u.label > 0; });
    console.log('root = ' + root.id);
    root.dfsFrom((u) => {
      if (u.parent != null) u.depth = u.parent.depth + 1;
      else u.depth = 0;
    });

    var furthest = null;
    this.node.forEach((u) => {
      if (u.label == 0) return;
      if (furthest == null || u.depth > furthest.depth) furthest = u;
    });

    if (furthest == null) throw 'furthest == null';

    furthest.dfsFrom((u) => {
      if (u.parent != null) u.depth = u.parent.depth + 1;
      else u.depth = 0;
    });

    furthest = null;
    this.node.forEach((u) => {
      if (u.label == 0) return;
      if (furthest == null || u.depth > furthest.depth) furthest = u;
    });

    if (furthest == null) throw 'furthest == null';
    return furthest.depth;
  };

  this.terminate = function() {
    return this.node.filter(u => u.label == 0).length == this.n;
  }
}

function optimalPlayer(g) {
  var diameter = g.findDiameter();

  var randomChoice = function(list) {
    var idx = Math.floor(Math.random() * list.length);
    return list[idx];
  };

  var takeRandomMove = function() {
    var hasLabel = [];
    g.node.forEach((u) => {
      if (u.label > 0) hasLabel.push(u);
    });

    if (hasLabel.length == 0) throw 'Empty list: hasLebel';
    var u = randomChoice(hasLabel);
    g.removeVertex(u.id);
    return u.id;
  };

  var chooseAction = function(criterion) {
    var candidate = [];
    var hasLabel = [];
    g.node.forEach((u) => {
      if (u.label == 0) return;
      hasLabel.push(u);
      var deg = 0;
      u.adj.forEach((v) => {
        if (v.label > 0) deg += 1;
      });
      if (criterion(deg)) candidate.push(u);
    });

    if (candidate.length == 0) {
      if (hasLabel.length != 1) throw 'List `hasLabel` has length different from 1';
      g.removeVertex(hasLabel[0].id);
      return hasLabel[0].id;
    } else {
      var u = randomChoice(candidate);
      g.removeVertex(u.id);
      return u.id;
    }
  };

  var res = -1;

  if (diameter % 3 == 1) {
    console.log('Losing state');
    res = takeRandomMove();
  } else {
    console.log('Winning state: diameter = ' + diameter);
    if (diameter % 3 == 0) res = chooseAction((deg) => { return deg > 1; });
    else res = chooseAction((deg) => { return deg == 1; });

    if (!g.terminate()) {
      diameter = g.findDiameter();
      if (diameter % 3 != 1) {
        throw 'Wrong diameter after removal';
      }
    }
  }
  return res;
}

function test() {
  var g = new tree(5);
  g.addEdge(0, 1);
  g.addEdge(1, 2);
  g.addEdge(2, 3);
  g.addEdge(3, 4);
  console.log(optimalPlayer(g));
  if (g.terminate()) {
    console.log('Game terminate');
    return;
  }
  g.removeVertex(2);
  if (g.terminate()) {
    console.log('Game terminate');
    return;
  }
  console.log(optimalPlayer(g));
}

//test();
//module.exports.treeNode = treeNode;
//module.exports.tree = tree;
//module.exports.optimalPlayer = optimalPlayer;

