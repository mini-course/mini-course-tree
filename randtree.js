// Xoshiro128**
// https://stackoverflow.com/questions/521295/seeding-the-random-number-generator-in-javascript/47593316#47593316
function xoshiro128ss(a, b, c, d) {
  return function() {
    var t = b << 9, r = a * 5; r = (r << 7 | r >>> 25) * 9;
    c ^= a; d ^= b;
    b ^= c; a ^= d; c ^= t;
    d = d << 11 | d >>> 21;
    return (r >>> 0) / 4294967296;
  }
}

function randtree(n, seed) {
  // Fixed seeds (generated randomly) in the three remaining "fields".
  // TODO: Properly seed the RNG.
  var rng = xoshiro128ss(seed, 1887679869, 2215286983, 394139938);
  var prufer = [];
  for (var i = 0; i < n - 2; i++) {
    prufer.push(Math.floor(rng() * n));
  }
  var degree = Array(n).fill(1);
  prufer.forEach((pru, _) => ++degree[pru]);
  console.log(prufer);
  var edges = [];
  prufer.forEach((prui, i) => {
    console.log(degree);
    for (var j = 0; j < n; j++) {
      if (degree[j] === 1) {
        edges.push({ source: prui, target: j });
        --degree[prui];
        --degree[j];
        break;
      }
    }
  });
  var last = [];
  degree.forEach((deg, idx) => {
    if (deg === 1) {
      last.push(idx);
    }
  });
  console.log(degree);
  console.assert(last.length === 2);
  edges.push({ source: last[0], target: last[1] });
  return edges;
}
